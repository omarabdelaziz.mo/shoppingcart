<?php
    // start session
    session_start();
 
    
    include 'cart.php';
    //initialize cart
    $cart = new Cart();
    
    // set page title
    $page_title="Cart";
    
    // include page header html
    include 'header.php';
    
    // contents will be here 
    $action = isset($_GET['action']) ? $_GET['action'] : "";
    
    echo "<div class='col-md-12'>";
        if($action=='removed')
        {
            echo "<div class='alert alert-info'>";
                echo "Product was removed from your cart!";
            echo "</div>";
        }
    
        else if($action=='quantity_updated')
        {
            echo "<div class='alert alert-info'>";
                echo "Product quantity was updated!";
            echo "</div>";
        }
    echo "</div>";
    if(count($_SESSION['cart'])>0)
    {
    
        // get the product ids
        $ids = array();
        foreach($_SESSION['cart'] as $id=>$value)
        {
          
            array_push($ids, $id);
        }
        
        //$stmt=$product->readByIds($ids);
        $stmt = $cart->get_products($ids);
        $total=0;
        $item_count=0;

        while ($row = mysqli_fetch_assoc($stmt))
        {

            extract($row);
            $quantity=$_SESSION['cart'][$id]['quantity'];
            $sub_total=$price*$quantity;
    
            echo "<div class='cart-row'>";
                echo "<div class='col-md-8'>";
        
                    echo "<div class='product-name m-b-10px'><h4>{$name}</h4></div>";
        
                    // update quantity
                    echo "<form class='update-quantity-form'>";
                        echo "<div class='product-id' style='display:none;'>{$id}</div>";
                        echo "<div class='input-group'>";
                            echo "<input type='number' name='quantity' value='{$quantity}' class='form-control cart-quantity' min='1' />";
                                echo "<span class='input-group-btn'>";
                                    echo "<button class='btn btn-default update-quantity' type='submit'>Update</button>";
                                echo "</span>";
                        echo "</div>";
                    echo "</form>";
        
                    // delete from cart
                    echo "<a href='remove_from_cart.php?id={$id}' class='btn btn-default'>";
                        echo "Delete";
                    echo "</a>";
                echo "</div>";
        
                echo "<div class='col-md-4'>";
                    echo "<h4>&#36;" . number_format($sub_total, 2, '.', ',') . "</h4>";
                echo "</div>";
            echo "</div>";
            
           
            $item_count += $quantity;
            $total+=$sub_total;
            $balance = $cart->update_balance($total);
            $previous_balance = $cart->get_previous_balance();  
        }
        //shipment
        echo '<div class="cart-row">';
            echo "<form  action='checkout.php' method='post' id='shipment'>";
                echo "<div class='form-group'>";
                    echo "<label for='shipment'>Select Shipment:</label>";
                    echo '<select class="form-control" id="shipment" name="shipment" >';                      
                        echo'<option  value="0">Select shipment</option>';
                        echo '<option value="1">Pick up</option>';
                        echo '<option value="2">UPS</option>';
                    echo '</select>';
                echo    "</div>";
                echo '<input type="submit"  name ="" class="btn btn primary" value="select shipment" style="display:none;">';
            
        echo '</div>';
        //displaying status
        echo "<div class='col-md-8'></div>";
        echo "<div class='col-md-12'>";
            echo "<div class='cart-row' style='text-align:center;'>";
                echo "<h4 class='m-b-10px'>Previous Blance (&#36;{$previous_balance})</h4>";
                echo "<h4 class='m-b-10px'>Total ({$item_count} items)</h4>";
                echo "<h4>Total price &#36; " . number_format($total, 2, '.', ',') . "</h4>";
                echo "<h4 class='m-b-10px'>Current Balance before shipment (&#36;{$balance})</h4>";
                if($balance < 0)
                {

                    
                    echo "<a href='checkout.php' class='btn btn-secondary btn-lg m-b-10px disabled'>";
                        echo "<span class='glyphicon glyphicon-shopping-cart'></span> Proceed to Checkout";
                    echo "</a>";
                    echo "<div class='col-md-12'>";
                    echo "<div class='alert alert-danger'>";
                        echo "You don't have enough Balance";
                    echo "</div>";
   
                }
                else if($balance > 0)
                {
                    echo "<button type='submit' class='btn btn-success btn-lg m-b-10px' id='checkout'>";
                        echo "<span class='glyphicon glyphicon-shopping-cart'></span> Proceed to Checkout";
                    echo "</button>";   
                }
                if(isset($_SESSION['message']))
                {
                    $error =  $_SESSION['message']; // display the message
                    echo "<div class='col-md-12'>";
                    echo "<div class='alert alert-danger'>";
                        echo $error;
                    echo "</div>";
                    unset($_SESSION['message']); // clear the value so that it doesn't display again
                }
                
            echo "</div>";
        echo "</div>";
        echo "</form>";
    }
    
    // no products were added to cart
    else
    {
        echo "<div class='col-md-12'>";
            echo "<div class='alert alert-danger'>";
                echo "No products found in your cart!";
            echo "</div>";
        echo "</div>";
    }
    // layout footer 
    include 'footer.php';
?>