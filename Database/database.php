<?php

//this class is reponsible for the database connection
class Database{
    private $host="localhost";
    private $username="root";
    private $password="123456";
    private $db_name="task";//database name
    private  $database_connection; // 
     
    
    public function __construct()
    {
      $this->database_connection = $this->database_connect($this->host, $this->username,$this->password);
      $this->database_select($this->db_name,$this->database_connection);
      
    }
    //applaying singletone design pattern
    /*
    public static function getConnection()
    {

        if(!isset($this->database_connection)){
            $this->database_connection  = self;
        }

        return $this->database_connection;

    }
    */
    /**
     * Connect to database
     */
    private function database_connect($database_host, $database_username, $database_password)
     {
        if ($connection = mysqli_connect($database_host, $database_username, $database_password))
         {
            return $connection;
            
        } 
        else 
        {
                die("Database connection error");
            
        }
    }
     /**
     * select a db
     */
    private function database_select($database_name) 
    {
        return mysqli_select_db($this->database_connection, $database_name)
            or die("No database is selecteted");
    }
    
    /**
     * Close db connection
     *
     */
    public   function database_close() {
        if(!mysqli_close($this->database_connection)) die ("Connection close failed.");
           
    }
    //Clean data that is used in SQL queries.
    function clean($str)
     {
		$str = trim($str); // remove 
                $str = stripslashes($str);
                $str = strip_tags($str);
		$str= mysql_real_escape_string($this->database_connection,$str);
                return $str;
                
    } 
    /**
     * Make query to the database
     * @param sql query
     * 
     * @return query result
     */
    public function database_query($database_query)
    {
       $this->encode();
       $query_result = mysqli_query($this->database_connection,$database_query);
       return $query_result;
    }

    /**
     * Executes query and returns query result (row, array)
     */
    
    
    public function get_row($query) 
    {
        if (!strstr(strtoupper($query), "LIMIT"))
            $query .= " LIMIT 0,1";
            $res =$this->database_query($query);
        if (!$res)
        {
         die( "Database error: " . mysqli_error($this->database_connection) . "<br/>In query: " . $query);
        }
        return mysqli_fetch_assoc($res);
    }
    public  function encode()
    {
        mysqli_query($this->database_connection,"SET NAMES utf8");
    }   

    /**
     * Executes query result (table, array of array)
     */
    public function database_all_array($database_result)
     {
        $array_return=array();
        while ($row = mysqli_fetch_array($database_result))
        {
            $array_return[] = $row;
        }
        if(count($array_return)>0)
        return $array_return;
    }

    /**
     * Executes query result (table, array of array)
     * @return associated array of rows 
     */
   public function database_all_assoc($database_result)
    {
        $array_return=array();
        while ($row = mysqli_fetch_assoc(mysqli_query($this->database_connection,$database_result)))
        {
            $array_return[] = $row;
        }
        return $array_return;
    }

    /**
     * Returns number of rows in the result
     *
     * @param mixed $database_result
     * @return integer
     */
    public function database_affected_rows($database_result)
    {

        return mysqli_affected_rows($database_result);
    }

    /**
     * Returns number of rows in the result
     *
     * @param mixed $database_result
     * @return integer
     */
    public function database_num_rows($database_result) {

        return mysqli_num_rows($database_result);
    }

    /*
    * desc: does an update query with an array
    * param: table, assoc array with data (not escaped), where condition (optional. if none given, all records updated)
    * returns: (query_id) for fetching results etc
    */
    public function update($table, $data, $where='1')
    {
        $q="UPDATE `$table` SET ";

        foreach($data as $key=>$val)
        {   
            if(strtolower($val)=='null') $q.= "`$key` = NULL, ";
            elseif(strtolower($val)=='now()') $q.= "`$key` =    (), ";
            else $q.= "`$key`='".$this->clean($val)."', ";
        }
        $q = rtrim($q, ', ') . ' WHERE '.$where.';';
    
    return $this->database_query($q);
    }
    /*
    * desc: does an insert query with an array
    * param: table, assoc array with data (not escaped)
    * @return   id of inserted record, false if error
    */
    public function insert($table, $data)
    {
        $q="INSERT INTO `$table` ";
        $v=''; $n='';
    
        foreach($data as $key=>$val)
        {
          
            $n.="`$key`, ";
            $v.= "'$val', ";
        }

        $q .= "(". rtrim($n, ', ') .") VALUES (". rtrim($v, ', ') .");";
        
      
        
        if($this->database_query($q))
        {
            return true;
        }
        else return false;

    }



}

?>