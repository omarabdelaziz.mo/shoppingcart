<?php 

// connect to database
include 'Database/database.php';

// include objects
include_once "product.php";
include_once "product_image.php";
class Cart{


    private $product;
    private $product_image;
    private $total=0;
    private $item_count=0;
    private static $currentBalance  = 100;
    private $previousBalance = 100;
    private $shipment  = ['Shipment','pickup', 'UPS'];
    public function __construct()
    {
     
        // get database connection
        $database = new Database();

    // initialize objects
        $this->product = new Product($database);
        $this->product_image = new ProductImage($database);
    }
    public function get_previous_balance()
    {
        return $this->previousBalance;
    }

    public function get_products($ids)
    {
        return $this->product->readByIds($ids);
    
    }
    public function update_balance($total)
    {
        $this->update_previous_balance(self::$currentBalance);
        return self::$currentBalance - $total;   
    }

    private function update_previous_balance($balance)
    {   
        if($balance >= 0)
        {
            $this->previousBalance = $balance;
        }
        
    }
    
}
?>