<?php

include_once 'Database/database.php';
class Product{
    private $conn;//database connection
    private $table_name="products";// the table name
    // object properties which is also columns from the table
    public $id;
    public $name;
    public $price;
    public $category_id;
    public $category_name;
    public $timestamp;

    //constructor
    public function __construct(Database $conn)
    {
        $this->conn = $conn;
    }
    

    /**
     * read all products
     * 
     * @return all products in the database
     */
     
    public function read()
    {
        // select all products query
        $query = "SELECT
                    products.id, products.name, products.price , AVG(product_rating.rating) AS rating
                FROM
                   " .$this->table_name." LEFT JOIN  product_rating
                    ON products.id = product_rating.product_id
                GROUP BY  products.id

                ";

       $result = $this->conn->database_query($query);
       
        // return values
        return $result;
    }
    
    public function count()
    {
 
        // query to count all product records
        $query = "SELECT count(*) FROM " . $this->table_name;
        
        $result = $this->conn->database_num_rows($query);
        return $result;
    }
    public function readByIds($ids)
    {
        //$ids_arr = str_repeat("'$ids',", count($ids) - 1) . "'$ids'";
        $ids_arr = array_fill(0, count($ids),array_map('intval',$ids));
        
        // query to select products
        $query = "SELECT id, name, price FROM products WHERE id IN (".implode(',', array_map('intval',$ids)).")";
        
        // prepare query statement
        $stmt = $this->conn->database_query($query);

        return $stmt;
    }

    public function setRate($product_id, $rating)
    {
        $data = array('product_id'=>$product_id, 'rating'=>$rating);
        
        return $this->conn->insert('product_rating',$data);
         
    }
    /**
     *  check if the product Exists
     * @param product Id
     * 
     * @return the number of row
     */
    public function productExists($product_id)
    {
        $q = "SELECT * FROM products WHERE id = '$product_id' ORDER BY name";
        
        return $this->conn->database_query($q)->num_rows;
        
    
    }
}    
?>