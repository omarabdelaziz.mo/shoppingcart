<?php

    // start session
    session_start();
 
    // connect to database
    include_once 'Database/database.php';
    

    // include objects
    include_once "product.php";
    
    include_once "product_image.php";
    
    // get database connection
    $database = new Database();

    
    // initialize objects
    $product = new Product($database);
    
    $product_image = new ProductImage($database);
    
    // to prevent undefined index notice
    $action = isset($_GET['action']) ? $_GET['action'] : "";
    
    //set page title
    $page_title="Products";
    
    // page header html
    include 'header.php';
    
    echo "<div class='col-md-12'>";
    if($action=='added')
    {
        echo "<div class='alert alert-info'>";
            echo "Product was added to your cart!";
        echo "</div>";
    }   
 
    if($action=='exists')
    {
        echo "<div class='alert alert-info'>";
            echo "Product already exists in your cart!";
        echo "</div>";
    }
    if(isset($_SESSION['message']))
    {
        $error =  $_SESSION['message']; // display the message
        echo "<div class='col-md-12'>";
        echo "<div class='alert alert-danger'>";
            echo $error;
        echo "</div>";
        unset($_SESSION['message']); // clear the value so that it doesn't display again
    }  
    echo "</div>";
    // Request data from the database
    // read all products in the database
    
    
    $stmt=$product->read();
    // count number of retrieved product
    $num = $stmt->num_rows;
    
    // if products retrieved were more than zero
    if($num > 0)
    {
        // needed for paging
        $page_url="products.php?";
        $total_rows=$product->count();
        
        // show products
        include_once "read_products.php";
         
    }
    
    // tell the user if there's no products in the database
    else
    {
        echo "<div class='col-md-12'>";
            echo "<div class='alert alert-danger'>No products found.</div>";
        echo "</div>";
    }

    // layout footer code
    include 'footer.php';
?>