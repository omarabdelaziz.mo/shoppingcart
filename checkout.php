<?php
// start session
    session_start();
    
    
    include 'cart.php';
    //initialize cart
    $cart = new Cart();
    // set page title
    $page_title="Checkout";
    
    // include page header html
    include 'header.php';
   
    (isset($_POST["shipment"])) ? $shipment = $_POST["shipment"] : $_POST["shipment"];
   
    if($shipment== 0)
    {

        header('Location: cart_process.php');
        $_SESSION['message'] = "pleace choose shipment." .$shipment;
    }
    else if($shipment == 1)
    {
        $shipmentPrice= 0;
    }
    else if($shipment== 2)
    {
        $shipmentPrice= 5;
    }
    if(count($_SESSION['cart'])>0)
    {
    
        // get the product ids
        $ids = array();
        foreach($_SESSION['cart'] as $id=>$value)
        {
            array_push($ids, $id);
        }
    
        $stmt=$cart->get_products($ids);
    
        $total=0;
        $item_count=0;
    
        while ($row = mysqli_fetch_assoc($stmt))
        {
            extract($row);
        
            $quantity=$_SESSION['cart'][$id]['quantity'];
            $sub_total=$price*$quantity;
        
            echo "<div class='cart-row'>";
                echo "<div class='col-md-8'>";
        
                    echo "<div class='product-name m-b-10px'><h4>{$name}</h4></div>";
                    echo $quantity>1 ? "<div>{$quantity} items</div>" : "<div>{$quantity} item</div>";
        
                echo "</div>";
        
                echo "<div class='col-md-4'>";
                    echo "<h4>&#36;" . number_format($sub_total, 2, '.', ',') . "</h4>";
                echo "</div>";
            echo "</div>";
        
            $item_count += $quantity;
            $total+=$sub_total;
            $balance = $cart->update_balance($total);
    
        }
        $balance = $cart->update_balance($total+$shipmentPrice);
        echo "<div class='col-md-12 text-align-center'>";
            echo "<div class='cart-row'>";
                
                if($item_count>1)
                {
                    //$balance = $balance - $shipmentPrice;
                    echo "<h4 class='m-b-10px'>Total ({$item_count} items)</h4>";
                    echo "<h4 class='m-b-10px'>Total Balance After shipement ({$balance} &#36;)</h4>";
                }else{
                    //$balance = $balance - $shipmentPrice;
                    echo "<h4 class='m-b-10px'>Total ({$item_count} item)</h4>";
                    echo "<h4 class='m-b-10px'>Total Balance After shipment({$balance} &#36;)</h4>";
                }
                echo "<h4>Total Price after shipment &#36;" . number_format($total+$shipmentPrice, 2, '.', ',') . "</h4>";
                echo "<a href='place_order.php' class='btn btn-lg btn-success m-b-10px'>";
                    echo "<span class='glyphicon glyphicon-shopping-cart'></span> Place Order";
                echo "</a>";
            echo "</div>";
        echo "</div>";
            
    }
    
    else
    {

        echo "<div class='col-md-12'>";
            echo "<div class='alert alert-danger'>";
                echo "No products found in your cart!";
            echo "</div>";
        echo "</div>";
    }
 
include 'footer.php';
?>