<?php


// connect to database
include_once 'Database/database.php';

class ProductImage{
    private $conn;//database connection
    private $table_name="product_images";// the table name

     // object properties
     public $id;
     public $product_id;
     public $name;
     public $timestamp;

    // constructor
    public function __construct(Database $conn)
    {
        $this->conn = $conn;
    }
    // read the first product image related to a product
    public function readFirst()
    {
    
        // select query
        $query = "SELECT id, product_id, name
                FROM " . $this->table_name . "
                WHERE product_id = '$this->product_id'
                ORDER BY name DESC";
    

        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        $stmt = $this->conn->database_query($query);
        
        // return values
        return $stmt;
    }
}

?>